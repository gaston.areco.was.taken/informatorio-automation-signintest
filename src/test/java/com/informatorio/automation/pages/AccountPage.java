package com.informatorio.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPage extends BasePage{
		
	public AccountPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(className="info-account")
	private WebElement confirmatioMessage;
	
	@FindBy(xpath="//a[@class=\"account\"]/span")
	private WebElement accountUserName;
	
	public String getMessage() {
		
		isElementPresent(2, confirmatioMessage);
		
		return confirmatioMessage.getText();
		
	}
	
	public String getAccountUserName() {
		
		isElementPresent(2, accountUserName);
		
		return accountUserName.getText();
		
	}
	
}
