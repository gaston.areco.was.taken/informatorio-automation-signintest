package com.informatorio.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.informatorio.automation.dto.User;

public class FillFormPage extends BasePage {
	
	public FillFormPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id="customer_firstname")
	private WebElement customerFirstName;
	
	@FindBy(id="customer_lastname")
	private WebElement customerLastName;
	
	@FindBy(id="passwd")
	private WebElement customerPasswd;
	
	@FindBy(id="firstname")
	private WebElement addressFirstName;
	
	@FindBy(id="lastname")
	private WebElement addressLastName;
	
	@FindBy(id="address1")
	private WebElement customerAddress;
	
	@FindBy(id="city")
	private WebElement customerCity;
	
	@FindBy(id="postcode")
	private WebElement customerPostcode;
	
	@FindBy(id="phone_mobile")
	private WebElement customerPhone;
	
	@FindBy(id="submitAccount")
	private WebElement registerButton;
	
	public void fillForm(User user) {
		
		isElementPresent(2, customerFirstName);
		
		RadioButtonPage radioButtonPage = new RadioButtonPage(driver);
		
		if (user.getGender().equalsIgnoreCase("Male")) {
			radioButtonPage.checkRadioButton1(true);
		}
		if (user.getGender().equalsIgnoreCase("Female")) {
			radioButtonPage.checkRadioButton2(true);
		}
		
		customerFirstName.sendKeys(user.getFirstName());
		customerLastName.sendKeys(user.getLastName());
		customerPasswd.sendKeys(user.getPassword());
		
		DropDownPage dropDownDays = new DropDownPage(driver);
		dropDownDays.selectOptionByVisibleTextDays(user.getBirthDay());
		DropDownPage dropDownMonths = new DropDownPage(driver);
		dropDownMonths.selectOptionByVisibleTextMonths(user.getBirthMonth());
		DropDownPage dropDownYears = new DropDownPage(driver);
		dropDownYears.selectOptionByVisibleTextYears(user.getBirthYear());
		
		customerAddress.sendKeys(user.getAddress());
		customerCity.sendKeys(user.getCity());
		
		DropDownPage dropDownState = new DropDownPage(driver);
		dropDownState.selectOptionByVisibleTextState(user.getState());
		
		customerPostcode.sendKeys(user.getZipCode());
		customerPhone.sendKeys(user.getPhoneNumber());
		
//		genderRadioButton.click();
//		customerFirstName.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		customerLastName.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		customerPasswd.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		customerBirthDay.click();
//		customerBirthDay.sendKeys(Keys.ARROW_DOWN);
//		customerBirthDay.sendKeys(Keys.ENTER);
//		customerBirthMonth.click();
//		customerBirthMonth.sendKeys(Keys.ARROW_DOWN);
//		customerBirthMonth.sendKeys(Keys.ENTER);
//		customerBirthYear.click();
//		customerBirthYear.sendKeys(Keys.ARROW_DOWN);
//		customerBirthYear.sendKeys(Keys.ENTER);
//		addressFirstName.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		addressLastName.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		customerAddress.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		customerCity.sendKeys(RandomStringUtils.randomAlphabetic(10));
//		customerState.click();
//		customerState.sendKeys(Keys.ARROW_DOWN);
//		customerState.sendKeys(Keys.ENTER);
//		customerPostcode.sendKeys("35006");
//		customerPhone.sendKeys("123");
			
	}
	
	public AccountPage clickRegister() {
		
		registerButton.click();
		return new AccountPage(driver);
		
	}
	
}
