package com.informatorio.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RadioButtonPage extends BasePage{
	
	@FindBy(id = "id_gender1")
	private WebElement radioButton1;
	
	@FindBy(id = "id_gender2")
	private WebElement radioButton2;
	

	public RadioButtonPage(WebDriver driver) {
		super(driver);
	}
	
	public void checkRadioButton1(boolean isChecked){
		if(isChecked^radioButton1.isSelected()) {
			radioButton1.click();
		}
	}
	
	public void checkRadioButton2(boolean isChecked){
		if(isChecked^radioButton2.isSelected()) {
			radioButton2.click();
		}
	}

}
