package com.informatorio.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class DropDownPage extends BasePage{
	
	@FindBy(id = "days")
	private WebElement dropDownDays;

	@FindBy(id = "months")
	private WebElement dropDownMonths;

	@FindBy(id = "years")
	private WebElement dropDownYears;
	
	@FindBy(id = "id_state")
	private WebElement dropDownState;
	
	public DropDownPage(WebDriver driver) {
		super(driver);
	}
	
	public void selectOptionByVisibleTextDays(String option){
		Select dropdownSelect = new Select(dropDownDays);
		dropdownSelect.selectByValue(option);
	}
	
	public void selectOptionByVisibleTextMonths(String option){
		Select dropdownSelect = new Select(dropDownMonths);
		dropdownSelect.selectByValue(option);
	}
	
	public void selectOptionByVisibleTextYears(String option){
		Select dropdownSelect = new Select(dropDownYears);
		dropdownSelect.selectByValue(option);
	}
	
	public void selectOptionByVisibleTextState(String option){
		Select dropdownSelect = new Select(dropDownState);
		dropdownSelect.selectByVisibleText(option);
	}

}
