package com.informatorio.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterMailPage extends BasePage {
	
	public RegisterMailPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id="email_create")
	private WebElement signInMail;
	
	@FindBy(id="SubmitCreate")
	private WebElement signInSubmitButton;
	
	@FindBy (id="email")
	private WebElement logInMail;
	
	@FindBy (id="passwd")
	private WebElement psswd;
	
	@FindBy (id="SubmitLogin")
	private WebElement logInSummitButton;
	
	public void writeNewMail (String mail) {
		
		signInMail.clear();
		signInMail.sendKeys(mail);
		
	}
	
	public FillFormPage submitEmail () {
		
		signInSubmitButton.click();
		return new FillFormPage(driver);
		
	}
	
	public void writeLogInMailAndPsswd (String mail, String pass) {
		
		logInMail.clear();
		logInMail.sendKeys(mail);
		psswd.clear();
		psswd.sendKeys(pass);
		
	}

	public AccountPage submitLogin () {
		
		logInSummitButton.click();
		return new AccountPage(driver);
		
	}
	
}
