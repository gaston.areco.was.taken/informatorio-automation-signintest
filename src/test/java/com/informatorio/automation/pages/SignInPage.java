package com.informatorio.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

	public SignInPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(className="login")
	private WebElement singInButton;
	
	public RegisterMailPage clickSingIn() {
		
		singInButton.click();
		return new RegisterMailPage(driver);
		
	}
	
	
}
