package com.informatorio.automation.test;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.informatorio.automation.dto.User;
import com.informatorio.automation.pages.AccountPage;
import com.informatorio.automation.pages.FillFormPage;
import com.informatorio.automation.pages.RegisterMailPage;
import com.informatorio.automation.pages.SignInPage;

public class SignInUserTest extends BaseTest{
	
	private String mail;
	private User user1;
	
	@BeforeSuite
	public void setSuite() {
		
		mail = "test" + Math.round(Math.random()*100000) + "@test.com.ar";
		user1 = new User("Male", "Some", "One", "qwerty12345", "8", "10", "1993", "Calle Sin Numero 123", "Los Angeles", "California", "12345", "54321", mail);
	
	}
	
	@Test
	public void createNewUser() {
		
		driver.get("http://automationpractice.com/index.php");
		
	    SignInPage signInPage = new SignInPage(driver);
		RegisterMailPage registerMailPage = signInPage.clickSingIn();
		registerMailPage.writeNewMail(mail);
		FillFormPage fillFormPage  = registerMailPage.submitEmail();
		fillFormPage.fillForm(user1);
		AccountPage accountPage = fillFormPage.clickRegister();
		
		assertEquals("Welcome to your account. Here you can manage all of your personal information and orders.", accountPage.getMessage());	
		
	}
	
	@Test
	public void logInTest() {
		
		driver.get("http://automationpractice.com/index.php");
		
		SignInPage signInPage = new SignInPage(driver);
		RegisterMailPage registerMailPage = signInPage.clickSingIn();
		registerMailPage.writeLogInMailAndPsswd(user1.getEmail(),user1.getPassword());
		AccountPage accountPage = registerMailPage.submitLogin();
				
		assertEquals(user1.getFirstName()+" "+user1.getLastName(), accountPage.getAccountUserName());	
			
	}
	
}
