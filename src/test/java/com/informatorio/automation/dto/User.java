package com.informatorio.automation.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class User {
	
	private String gender;
	private String firstName;
	private String lastName;
	private String password;
	private String birthDay;
	private String birthMonth;
	private String birthYear;
	private String address;
	private String city;
	private String state;
	private String zipCode;
	private String phoneNumber;
	private String email;
	
}
